﻿using M2Link.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace M2Link.Models
{
    public class LoginController : Controller
    {
        // GET: Form
        public ActionResult Form()
        {
            return View();
        }

        // POST: Form
        [HttpPost]
        public ActionResult Form(LoginModel model)
        {
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                if (!repo.checkUser(model.nickname, model.password))
                {
                    ModelState.AddModelError("password", "Veuillez entrer des identifiants valides.");
                }
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.nickname, true);
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        [HttpGet]
        public ActionResult Disconnect()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}