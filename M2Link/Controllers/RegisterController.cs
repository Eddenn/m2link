﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace M2Link.Controllers
{
    public class RegisterController : Controller
    {
        const string Password_Pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*/d)(?=.*[@$!%*?&])[A-Za-z/d@$!%*?&]{8,}$";

        // GET: Form
        public ActionResult Form() => View();

        // POST : Form
        [HttpPost]
        public ActionResult Form(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                User user = new User()
                {
                    first_name = model.first_name,
                    last_name = model.last_name,
                    email = model.email,
                    nickname = model.nickname,
                    password = model.password
                };

                using (M2LinkContext context = new M2LinkContext())
                {
                    UserRepository repo = new UserRepository(context);
                    repo.Add(user);
                    context.SaveChanges();
                }
                FormsAuthentication.SetAuthCookie(model.nickname, true);
                return RedirectToAction("Index", "Home");
            }
        }
    }
}