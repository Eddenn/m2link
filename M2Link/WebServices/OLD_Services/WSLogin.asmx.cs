﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.WebServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace M2Link.WebServices
{
    /// <summary>
    /// Description résumée de WSLogin
    /// </summary>
    [WebService(Namespace = "http://jaipasduriamettre.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    // [System.Web.Script.Services.ScriptService]
    public class WSLogin : System.Web.Services.WebService
    {
        [WebMethod]
        public UserModel Validate(string nickname, string password)
        {
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                if (!repo.checkUser(nickname, password))
                {
                    return null;
                }
                else
                {
                    User user = repo.getUser(nickname);
                    UserModel userModel = new UserModel()
                    {
                        nickname = user.nickname,
                        email = user.email,
                        first_name = user.first_name,
                        last_name = user.last_name
                    };
                    return userModel;
                }
            }
        }
    }
}
