﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using M2Link.Repository;
using M2Link.WebServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "WSMessage" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez WSMessage.svc ou WSMessage.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class WSMessage : IWSMessage
    {
        public List<MessageModel> Get_followed_users_messages(string nickname)
        {
            List<MessageModel> messages = new List<MessageModel>();

            using (M2LinkContext context = new M2LinkContext())
            {
                MessageRepository msgRepo = new MessageRepository(context);

                UserRepository userRepo = new UserRepository(context);
                List<string> list_follow = userRepo.getFollowedUsersNicknames(nickname);

                foreach (Message msg in msgRepo.getMessages(list_follow))
                {
                    MessageModel msgModel = new MessageModel()
                    {
                        guid = msg.guid,
                        author = msg.author.nickname,
                        date = msg.date,
                        message = msg.message
                    };
                    messages.Add(msgModel);
                }
            }
            return messages;
        }

        public ActionResponse Send_message(string author_nickname, string message)
        {
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository userRepo = new UserRepository(context);
                User user = userRepo.getUser(author_nickname);
                if (user == null)
                {
                    return new ActionResponse(404, "User not found.");
                }
                Message msg = new Message()
                {
                    author = user,
                    date = DateTime.Now,
                    guid = Guid.NewGuid(),
                    message = message
                };

                MessageRepository msgRepo = new MessageRepository(context);
                msgRepo.Add(msg);
                user.messages.Add(msg);

                context.SaveChanges();
            }
            return new ActionResponse(200, "Message sended.");
        }
    }
}
