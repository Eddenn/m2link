﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class ProfileModel
    {
        [Required]
        [Display(Name = "Name")]
        public string last_name { get; set; }

        [Required]
        [Display(Name = "Prenom")]
        public string first_name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Adresse email")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Pseudo")]
        public string nickname { get; set; }

        [Display(Name = "Messages")]
        public List<MessageModel> messages { get; set; }

        [Display(Name = "Utilisateurs suivis")]
        public List<string> followed_users { get; set; }

        public ProfileModel()
        {
            this.messages = new List<MessageModel>();
            this.followed_users = new List<string>();
        }
    }
}