﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class RegisterModel
    {
        [Required]
        [Display(Name = "Name")]
        public string last_name { get; set; }

        [Required]
        [Display(Name = "Prenom")]
        public string first_name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Adresse email")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Pseudo")]
        public string nickname { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Verif. du mot de passe")]
        [Compare("password")]
        public string verify_password { get; set; }

        public RegisterModel() { }
        
    }
}