﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.Repository
{
    public class FollowRepository
    {
        public M2LinkContext context { get; set; }

        public FollowRepository(M2LinkContext context)
        {
            this.context = context;
        }

        public List<string> getFollowedUsersNickname(string user)
        {
            List<string> followed_users = new List<string>();
            foreach(Follow f in context.Follows.Where(follow => follow.user == user)) {
                followed_users.Add(f.followed_user);
            }
            return followed_users;
        }

        public List<Follow> getFollowedUsers(string user)
        {
            return context.Follows.Where(follow => follow.user == user).ToList();
        }

        public void Follow(string user, string user_to_follow)
        {
            if(!getFollowedUsersNickname(user).Contains(user_to_follow))
            {
                Add(new Follow(user, user_to_follow));
            }
        }

        public void Unfollow(string user, string user_to_unfollow)
        {
            if (getFollowedUsersNickname(user).Contains(user_to_unfollow))
            {
                Delete(context.Follows.First(follow => follow.user == user && follow.followed_user == user_to_unfollow));
            }
        }

        public void Add(Follow follow)
        {
            context.Follows.Add(follow);
        }

        public void Delete(Follow follow)
        {
            context.Follows.Remove(follow);
        }

        public void Delete(string user)
        {
            foreach(Follow f in getFollowedUsers(user))
            {
                context.Follows.Remove(f);
            }
        }
    }
}